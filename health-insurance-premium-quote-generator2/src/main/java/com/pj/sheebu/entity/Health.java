package com.pj.sheebu.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "HEALTH")
public class Health {


	@Id
    @Column(name="health_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	private int id;
	private boolean hypertension;
	private boolean bloodPressure;
	private boolean bloodSugar;
	private boolean overweight;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public boolean isHypertension() {
		return hypertension;
	}
	public void setHypertension(boolean hypertension) {
		this.hypertension = hypertension;
	}
	public boolean isBloodPressure() {
		return bloodPressure;
	}
	public void setBloodPressure(boolean bloodPressure) {
		this.bloodPressure = bloodPressure;
	}
	public boolean isBloodSugar() {
		return bloodSugar;
	}
	public void setBloodSugar(boolean bloodSugar) {
		this.bloodSugar = bloodSugar;
	}
	public boolean isOverweight() {
		return overweight;
	}
	public void setOverweight(boolean overweight) {
		this.overweight = overweight;
	}
	
	
	
	

	
	
	

}
