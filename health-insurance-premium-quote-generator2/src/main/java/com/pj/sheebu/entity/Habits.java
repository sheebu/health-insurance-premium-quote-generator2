package com.pj.sheebu.entity;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "HABITS")
public class Habits {
	@Id
	@Column(name = "habit_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private boolean smoking;
	private boolean Alcohol;
	private boolean dailyExercise;
	private boolean drugs;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public boolean isSmoking() {
		return smoking;
	}
	public void setSmoking(boolean bmoking) {
		this.smoking = bmoking;
	}
	public boolean isAlcohol() {
		return Alcohol;
	}
	public void setAlcohol(boolean alcohol) {
		Alcohol = alcohol;
	}
	public boolean isDailyExercise() {
		return dailyExercise;
	}
	public void setDailyExercise(boolean dailyExercise) {
		this.dailyExercise = dailyExercise;
	}
	public boolean isDrugs() {
		return drugs;
	}
	public void setDrugs(boolean drugs) {
		this.drugs = drugs;
	}
	
	
	
}
