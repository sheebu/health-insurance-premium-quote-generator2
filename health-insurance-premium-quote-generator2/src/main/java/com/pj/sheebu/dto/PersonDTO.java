package com.pj.sheebu.dto;

import java.util.HashMap;
import java.util.Map;

public class PersonDTO {

	private int id;

	private String name;

	private String gender;

	private Integer age;
	private HabitDTO habitDTO;
	private HealthDTO healthDTO;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public HabitDTO getHabitDTO() {
		return habitDTO;
	}

	public void setHabitDTO(HabitDTO habitDTO) {
		this.habitDTO = habitDTO;
	}

	public HealthDTO getHealthDTO() {
		return healthDTO;
	}

	public void setHealthDTO(HealthDTO healthDTO) {
		this.healthDTO = healthDTO;
	}
	

}
