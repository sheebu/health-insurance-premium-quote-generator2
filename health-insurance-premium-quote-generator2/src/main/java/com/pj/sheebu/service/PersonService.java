package com.pj.sheebu.service;

import java.util.List;

import javax.transaction.Transactional;

import com.pj.sheebu.dao.PersonDAO;
import com.pj.sheebu.dto.PersonDTO;
import com.pj.sheebu.entity.Person;
import com.pj.sheebu.entity.Habits;

public interface PersonService {

	void setPersonDAO(PersonDAO personDAO);

	void addPerson(PersonDTO p);
	public double calculateInsurance(PersonDTO p);

}