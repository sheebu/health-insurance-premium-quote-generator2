package com.pj.sheebu.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.springframework.stereotype.Service;


import com.pj.sheebu.dao.PersonDAO;
import com.pj.sheebu.dto.PersonDTO;
import com.pj.sheebu.entity.Health;
import com.pj.sheebu.entity.Person;
import com.pj.sheebu.utilities.DtoEntityTransfomers;
import com.pj.sheebu.utilities.PremiumCalculaters;
import com.pj.sheebu.entity.Habits;

@Service
public class PersonServiceImpl implements  PersonService {
	
     
    private PersonDAO personDAO;
   
    
    

	

	/* (non-Javadoc)
	 * @see net.nestgroup.webmvc.service.PersonService#setPersonDAO(net.nestgroup.webmvc.dao.PersonDAO)
	 */

	public void setPersonDAO(PersonDAO personDAO) {
        this.personDAO = personDAO;
    }
 
    /* (non-Javadoc)
	 * @see net.nestgroup.webmvc.service.PersonService#addPerson(net.nestgroup.webmvc.entity.Person)
	 */
 
	
	@Transactional
	public void addPerson(PersonDTO p) {
        this.personDAO.addPerson(DtoEntityTransfomers.personEntityFromPersonDto(p));
      
      
    }
    
    /* (non-Javadoc)
	 * @see net.nestgroup.webmvc.service.PersonService#listPersons()
	 */
	@Transactional
	public double calculateInsurance(PersonDTO personDto) {
        Person p=DtoEntityTransfomers.personEntityFromPersonDto(personDto);
        double premium=PremiumCalculaters.calculatePremium(p.getAge(),p.getGender());
        premium=PremiumCalculaters.calculatePremiumBasedOnHealth(p.getHealth(),premium);
        return PremiumCalculaters.calculatePremiumBasedOnHabit(p.getHabits(),premium);
      
      
    }
	
	
 
   
 
}
