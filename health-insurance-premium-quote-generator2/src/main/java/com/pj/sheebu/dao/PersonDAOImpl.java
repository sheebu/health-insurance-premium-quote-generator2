package com.pj.sheebu.dao;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.springframework.stereotype.Repository;

import com.pj.sheebu.entity.Health;
import com.pj.sheebu.entity.Person;
import com.pj.sheebu.entity.Habits;

@Repository
public class PersonDAOImpl implements PersonDAO {

	// private static final Logger logger =
	// LoggerFactory.getLogger(PersonDAOImpl.class);

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}


	public void addPerson(Person p) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(p);

	}


	public void updatePerson(Person p) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(p);

		// logger.info("Person updated successfully, Person Details="+p);
	}

	@SuppressWarnings("unchecked")
	
	public List<Person> listPersons() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Person> personsList = session.createQuery("from Person").list();

		return personsList;
	}

	


	public Person getPersonById(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Person p = (Person) session.load(Person.class, new Integer(id));
		return p;
	}


	

	
	public void removePerson(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Person p = (Person) session.load(Person.class, new Integer(id));
		if (null != p) {
			session.delete(p);
		}

	}

	
	
	
	

}
