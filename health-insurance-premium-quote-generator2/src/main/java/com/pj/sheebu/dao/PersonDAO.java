package com.pj.sheebu.dao;

import java.util.List;

import com.pj.sheebu.entity.Health;
import com.pj.sheebu.entity.Person;
import com.pj.sheebu.entity.Habits;

public interface PersonDAO {
	 public void addPerson(Person p);
	    public void updatePerson(Person p);
	    public List<Person> listPersons();
	    public Person getPersonById(int id);
	    public void removePerson(int id);
	    
		

}
