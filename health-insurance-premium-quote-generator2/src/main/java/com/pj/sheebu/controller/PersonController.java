package com.pj.sheebu.controller;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.pj.sheebu.dto.PersonDTO;
import com.pj.sheebu.entity.Person;
import com.pj.sheebu.service.PersonService;
import com.pj.sheebu.utilities.DtoEntityTransfomers;

@Controller
public class PersonController {
     

    private PersonService personService;
     
    @Autowired(required=true)
    @Qualifier(value="personService")
    public void setPersonService(PersonService ps){
        this.personService = ps;
    }
     
    @RequestMapping(value = "/person", method = RequestMethod.GET)
    public String person(Model model) {
        model.addAttribute("person", new Person());
        Map<String,String> gender = new LinkedHashMap<String,String>();
    	gender.put("M", "Male");
    	gender.put("F", "Female");
        return "person";
    }
    
   
     
  
    
    @RequestMapping(value= "/person/calculate", method = RequestMethod.POST)
    public String addPerson(@ModelAttribute("person") Person p, Model model){
         
       PersonDTO personDTO=DtoEntityTransfomers.personDtoFromEntityPerson(p);
            this.personService.addPerson(personDTO);
        
            p.setPremium(personService.calculateInsurance(personDTO));
            
      
        return  "InsuranceQuate";
         
    }
    @ModelAttribute("gender")
    protected Map referenceData(HttpServletRequest request) throws Exception {
    	//Map referenceData = new HashMap();
    	Map<String,String> gender = new LinkedHashMap<String,String>();
    	gender.put("M", "Male");
    	gender.put("F", "Female");
    	
    	//referenceData.put("gender", gender);
    	return gender;
    }
     
   
  
     
}
